
//
//  ParseTutorial-Bridging-Header.h
//  MyBookStand
//
//  Created by Alexandre Martinez Olmos on 20/4/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//


//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/BFTask.h>