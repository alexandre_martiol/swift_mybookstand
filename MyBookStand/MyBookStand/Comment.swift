//
//  Comment.swift
//  MyBookStand
//
//  Created by Alexandre Martinez Olmos on 7/5/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class Comment: PFObject {
    var text: String = ""
    var author: String = ""
    var num_favs: Int = 0
    var book_points: Int = 0
    var book_id: String = ""
}
