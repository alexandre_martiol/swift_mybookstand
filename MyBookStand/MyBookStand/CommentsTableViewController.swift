//
//  CommentsTableViewController.swift
//  MyBookStand
//
//  Created by Alexandre Martinez Olmos on 6/5/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class CommentsTableViewController: PFQueryTableViewController {
    var book_id: String?
    var currentObject : PFObject?
    
    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryTableView
        self.parseClassName = "Comment"
        self.textKey = "text"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
    }
    
    // Define the query that will provide the data for the table view
    override func queryForTable() -> PFQuery {
        var query = PFQuery(className: "Comment")
        query.whereKey("book_id", equalTo: book_id!)
        query.orderByAscending("text")
        return query
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("CommentCell") as! PFTableViewCell!
        
        if cell == nil {
            cell = PFTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "CommentCell")
        }
        
        // Extract values from the PFObject to display in the table cell
        if let text = object["text"] as? String {
            cell?.textLabel?.text = text
        }
        
        if let author = object["author"] as? String {
            cell?.detailTextLabel?.text = "Author:  \(author)"
        }
       
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "BackToDetail") {
            println(self.currentObject)
            
            // Get the new view controller using [segue destinationViewController].
            var detailScene = segue.destinationViewController as! DetailViewController
            
            //We have to return the object to the previous view because if we don't do this,
            //we will loose the information and the DetailView will appear empty.
            detailScene.currentObject = self.currentObject
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        // Refresh the table to ensure any data changes are displayed
        tableView.reloadData()
    }
}
