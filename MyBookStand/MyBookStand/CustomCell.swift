//
//  CustomCell.swift
//  MyBookStand
//
//  Created by Alexandre Martinez Olmos on 20/4/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//


import UIKit

class CustomCell:  PFTableViewCell {
    @IBOutlet weak var bookCover: PFImageView!
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookPoints: UILabel!
    @IBOutlet weak var bookComments: UILabel!
    
    
}