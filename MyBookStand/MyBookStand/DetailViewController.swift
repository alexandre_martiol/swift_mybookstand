//
//  DetailViewController.swift
//  MyBookStand
//
//  Created by Alexandre Martinez Olmos on 20/4/15.
//  Copyright (c) 2015 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var favsLabel: UILabel!
    
    var currentObject : PFObject?
    var book_id: String?
    var book_title: String!
    var punctuation = 0
    
    @IBOutlet weak var bookCover: PFImageView!
    @IBOutlet weak var bookTitle: UILabel!
    @IBOutlet weak var bookAuthor: UILabel!
    @IBOutlet weak var bookGenre: UILabel!
    @IBOutlet weak var bookEditorial: UILabel!
    @IBOutlet weak var bookIsbn: UILabel!
    @IBOutlet weak var newCommentText: UITextView!
    @IBOutlet weak var pointsSlider: UISlider!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //This tap will be used to finsh to write the comment and close the keyboard
        var tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "DismissKeyboard")
        view.addGestureRecognizer(tap)
        
        //The keyboard is showed above the view. So we have to move the view up or down.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);

        self.showInfoBook()
    }
    
    func showInfoBook() {
        // Do any additional setup after loading the view.
        if let object = currentObject {
            if let image = object["cover"] as? PFFile {
                bookCover.file = image
                bookCover.loadInBackground()
            }
            
            if let title = object["title"] as? String {
                bookTitle.text = "Title: \(title)"
                self.book_title = title
            }
            
            if let author = object["author"] as? String {
                bookAuthor.text = "Author: \(author)"
            }
            
            if let genre = object["genre"] as? String {
                bookGenre.text = "Genre: \(genre)"
            }
            
            if let editorial = object["editorial"] as? String {
                bookEditorial.text = "Editorial: \(editorial)"
            }
            
            if let isbn = object["isbn"] as? String {
                bookIsbn.text = "ISBN: \(isbn)"
            }
            
            if let points = object["punctuation"] as? Int {
                favsLabel.text = "\(points)"
            }
            
            if let comments = object["num_comments"] as? Int {
                commentsLabel.text = "\(comments)"
            }
            
            if let id = object.objectId as String! {
                self.book_id = id
            }
        }
    }
    
    @IBAction func backAction(sender: AnyObject) {
        var storyboard : UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
        var vc : TableViewController = storyboard.instantiateViewControllerWithIdentifier("TableViewController") as! TableViewController
        
        let navigationController = UINavigationController(rootViewController: vc)
        
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func punctuationAction(sender: UISlider) {
        var currentValue = Int(sender.value)
        
        switch currentValue {
        case 1:
            star1.hidden = false
            star2.hidden = true
            star3.hidden = true
            star4.hidden = true
            star5.hidden = true
            
            self.punctuation = 1
            
        case 2:
            star1.hidden = false
            star2.hidden = false
            star3.hidden = true
            star4.hidden = true
            star5.hidden = true
            
            self.punctuation = 2
            
        case 3:
            star1.hidden = false
            star2.hidden = false
            star3.hidden = false
            star4.hidden = true
            star5.hidden = true
            
            self.punctuation = 3
            
        case 4:
            star1.hidden = false
            star2.hidden = false
            star3.hidden = false
            star4.hidden = false
            star5.hidden = true
            
            self.punctuation = 4
            
        case 5:
            star1.hidden = false
            star2.hidden = false
            star3.hidden = false
            star4.hidden = false
            star5.hidden = false
            
            self.punctuation = 5
            
        default:
            star1.hidden = true
            star2.hidden = true
            star3.hidden = true
            star4.hidden = true
            star5.hidden = true
            
            self.punctuation = 0
        }
    }
    
    @IBAction func addNewComment(sender: AnyObject) {
        var newComment = PFObject(className: "Comment")
        newComment.setObject(newCommentText.text, forKey: "text")
        newComment.setObject("Unknown", forKey: "author")
        newComment.setObject(0, forKey: "num_favs")
        newComment.setObject(punctuation, forKey: "book_points")
        newComment.setObject(book_id!, forKey: "book_id")
        newComment.save()
        
        if let object = currentObject {
            var num_comments = 0
            var points = 0
            
            if let num = object["num_comments"] as? Int {
                num_comments = num
            }
            
            if let p = object["punctuation"] as? Int {
                points = p
            }
            
            points = ((points + punctuation) / 2)
            
            object["num_comments"] = num_comments + 1
            object["punctuation"] = points
            
            // Save the data back to the server in a background task
            object.saveEventually(nil)
            
        }
        
        // Return to table view
        // Get the new view controller using [segue destinationViewController].
        var storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var vc : CommentsTableViewController = storyboard.instantiateViewControllerWithIdentifier("CommentsTableViewController") as! CommentsTableViewController
        
        vc.book_id = book_id
        vc.currentObject = self.currentObject
        
        self.presentViewController(vc, animated: true, completion: nil)
    }
    
    @IBAction func shareButton(sender: UIBarButtonItem) {
        let textToShare = "I like the book \(self.book_title)!! Do you like too??"
        var array = [textToShare]
        
        var controller = UIActivityViewController(activityItems: array, applicationActivities: nil)
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    //Calls this function when the tap is recognized.
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y -= 150
    }
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y += 150
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "CommentsTableSegue") {
            let viewController:CommentsTableViewController = segue.destinationViewController as! CommentsTableViewController
            
            
            viewController.book_id = book_id
            viewController.currentObject = self.currentObject
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
